# Cluster Management

The purpose of this project is to manage the GitLab Managed Apps installation into the kubernetes clusters.

See GitLab Documentation [here](https://docs.gitlab.com/ee/user/clusters/applications.html#install-using-gitlab-ci-alpha). 